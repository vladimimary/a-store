# A-Store

Учебный проект онлайн-магазина мерча для Альфа-Банка. Аналог https://store.alfabank.ru/

- **Главная страница**:
    Должна содержать в себе ссылки на страницы с товарами.
    
        
- Страница **Сделано в Альфе**:
    Должна содержать список с товарами.
      
- Страница **Свой дизайн**:
    Всё аналогично предыдущей странице, но товары разбиты по группам.
    
- Страница **Товара**:
    Галерея с картинками, описание, форма добавления в корзину.
        
- Страница **Корзины**:
    Форма оформления заказа.

Каждая страница должна содержать в себе хэдер с выезжающим меню и футер. Если в корзине лежит какой-то товар, также должна отображаться иконка с корзиной.


## Стэк

- React, TypeScript и Redux Toolkit;
- Инструменты тестирования Jest и react testing library;
- Библиотеку для роутинга React Router v6;
- Библиотеку компонентов Альфа Банка
- Библиотека для работы с формами: Formik и react-hook-form.