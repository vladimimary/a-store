import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { BasketType, ProductStateType } from "./types";

const initialState: BasketType = { list: [] };

const NAME = "basket";

const basketSlice = createSlice({
  name: NAME,
  initialState: initialState,
  reducers: {
    addProduct(state, action: PayloadAction<ProductStateType>) {
      state.list.push(action.payload);
    },
    addCount(state, action: PayloadAction<string>) {
      const i = state.list.findIndex((item) => item.id === action.payload);
      state.list[i].count++;
    },
    deleteCount(state, action: PayloadAction<string>) {
      const i = state.list.findIndex((item) => item.id === action.payload);
      state.list[i].count--;
    },
    deleteProduct(state, action: PayloadAction<string>) {
      const i = state.list.findIndex((item) => item.id === action.payload);
      state.list.splice(i, 1);
    },
  },
});

export const { addProduct, addCount, deleteCount, deleteProduct } =
  basketSlice.actions;

export default basketSlice.reducer;
