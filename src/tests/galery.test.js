import { Galery } from "../components/product-info/galery";
import { render, fireEvent, screen } from "@testing-library/react";

describe("Check click on image", () => {
  test("Main image must be  clicked image", () => {
    const images = [
      "https://cont.ws/uploads/pic/2022/3/подмигиваем%20%281%29.jpg",
      "https://omoro.ru/wp-content/uploads/2018/05/smailik-ylibka-1.jpg",
    ];

    render(<Galery images={images} />);
    const image1 = screen.getByTestId(1);
    let imageMain = screen.getByTestId("mainImage");

    fireEvent.click(image1);

    expect(imageMain.src).toBe(image1.src);
  });
});
