import { getGroup } from "../utils/parsers";

describe("Title of group", () => {
    
  test("Title of sdelano-v-alfe must be Сделано в Альфе", () => {
    const productGroup = "sdelano-v-alfe";

    const res = getGroup(productGroup);

    expect(res.head.title).toBe("Сделано в Альфе");
  });

  test("Title of svoy-dizain must be Свой дизайн", () => {
    const productGroup = "svoy-dizain";

    const res = getGroup(productGroup);

    expect(res.head.title).toBe("Свой дизайн");
  });

  test("Title of other must be Страница не найден", () => {
    const productGroup = 345;

    const res = getGroup(productGroup);

    expect(res.head.title).toBe("Страница не найдена");
  });
});
