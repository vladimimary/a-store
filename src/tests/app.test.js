import { render as rtlRender, screen, fireEvent } from "@testing-library/react";
import "@testing-library/jest-dom";
import { App } from "../components/app";


const render = (ui, {route = '/'} = {}) => {
    window.history.pushState({}, 'Test page', route)
  
    return rtlRender(ui)
  }

describe("Check Main page component", () => {
  test("Made in Alfa must be on the main page", () => {
    render(<App />);
    expect(screen.getByText("Сделано в Альфе")).toBeInTheDocument;
  });

  test("Check My Design must be on the main page", () => {
    render(<App />);
    expect(screen.getByText("Свой дизайн")).toBeInTheDocument;
  });

  test("Check header must be on the main page", () => {
    render(<App />);
    expect(screen.getByTestId("header")).toBeInTheDocument;
  });

  test("Check footer must be on the main page", () => {
    render(<App />);
    expect(screen.getByTestId("footer")).toBeInTheDocument;
  });
});

describe("Check route", () => {
    

  test("route to Made in Alfa", () => {
    render(<App />, { route: "/" });
    fireEvent.click(screen.getByText("Сделано в Альфе"));
    expect(
      screen.getByText("Хотим каждую из этих вещей! Себе, родным и друзьям")
    ).toBeInTheDocument();
    expect(screen.getByTestId("header")).toBeInTheDocument;
  });

  test("route to My design", () => {
    render(<App />, { route: "/" });
    fireEvent.click(screen.getByText("Свой дизайн"));
    expect(
      screen.getByText(
        /Выберите вещь, а затем - цвет, размер и стикер/
      )
    ).toBeInTheDocument();
    expect(screen.getByTestId("header")).toBeInTheDocument;
  });

  test("route to Non-existent page", () => {
    render(<App />, { route: "/empty" });
    expect(screen.getByText("Страница не найдена")).
    toBeInTheDocument();expect(screen.getByTestId("header")).toBeInTheDocument;
  });
});
