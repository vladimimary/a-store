import "./index.css"
import React, { useCallback} from 'react';
import { Link } from '@alfalab/core-components/link';
import { BurgerMIcon } from '@alfalab/icons-glyph/BurgerMIcon';
import { Drawer } from '@alfalab/core-components/drawer';
import { Button } from '@alfalab/core-components/button';
import { Navigation } from "../navigation";

export const Header = () => {
    const [open, setOpen] = React.useState(false);
    const handleModalOpen = useCallback(() => setOpen((prev) => !prev), []);
    return(
        <header data-testid="header">
            <Link
                data-testid="test"
                className="header_text header_text_red"
                view="default"
                rel="noopen"
                href="/"
            >
                <p className="header_text header_text_red">A-Store</p>
            </Link>
            <div className="header_menu">
                <Button 
                    view="ghost" 
                    onClick={handleModalOpen}
                    leftAddons={<BurgerMIcon color="black" />}>
                        <p className="header_text header_text_black">меню</p>
                </Button>
                <Drawer open={open} onClose={handleModalOpen}><Navigation handleClick={handleModalOpen}/></Drawer>
            </div>
        </header>
    )
};