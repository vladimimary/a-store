import "./index.css";
import { YMaps, Map, Placemark } from "@pbe/react-yandex-maps";

export const Contacts = () => {
  return (
    <div className="contacts">
      <h1 className="contacts-title">Контакты</h1>
      <div className="contacts-text">
        <h4>+7 906 061 60 20 {"\n"} info@alfabankstore.ru</h4>
        <h4>г. Москва, пр-т Андропова, 18 корп. 3</h4>
        <h4>
          пн-чт:{"\n"}10:00—19:00{"\n"}пт:{"\n"}10:00—17:30{" "}
        </h4>
        <h4>Принимаем к оплате карты Visa, Mastercard, МИР.</h4>
      </div>
      <YMaps>
        <Map
          width={800}
          height={500}
          defaultState={{
            center: [55.692353, 37.663166],
            zoom: 15,
            controls: ["zoomControl", "fullscreenControl"],
          }}
          modules={["control.ZoomControl", "control.FullscreenControl"]}
        >
          <Placemark defaultGeometry={[55.692353, 37.663166]} />
        </Map>
      </YMaps>
      ;
    </div>
  );
};
