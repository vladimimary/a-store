import { useAppSelector } from "../../store";
import { PfmBasketMIcon } from "@alfalab/icons-glyph/PfmBasketMIcon";
import "./index.css";
import { Circle } from "@alfalab/core-components/icon-view/circle";
import { Button } from "@alfalab/core-components/button";

export const BasketIndicator = () => {
  const basket = useAppSelector((state) => state.basket.list);
  const count = basket.reduce(
    (sum, current) => sum + current.count,
    0
  );
  return (
    <div className="basket-indicator">
     {(count !==0) && <Button href="/basket" view="ghost">
        <Circle
          size={80}
          backgroundColor="#ef3124"
          bottomAddons={<p className="basket-indicator-count">{count}</p>}
        >
          <PfmBasketMIcon color="#ffffff" />
        </Circle>
      </Button>}
    </div>
  );
};
