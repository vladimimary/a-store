import { ProductsType } from '../types'
import { Product } from './product'
import { useMemo } from 'react'

export const ProductsList = ({products, title, description} : ProductsType) => {
    const availProducts = useMemo(() => products.filter(item => item.availability ), [products])
    
    return (
        <div className="products">
            <div className='products-head'>
                <h1 className='products-h products-h-title'>{title}</h1>
                <h4 className='products-h products-h-subtitle'>{description}</h4>
            </div>
            <div className='product-cards'>
                {(!availProducts.length) 
                    ? <h1>Товаров не найдено</h1> 
                    : availProducts.map((item) => <Product key={item.id} {...item}/>)}
            </div>
        </div>
    )
}