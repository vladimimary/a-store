import { ProductType } from "../../types";
import { Amount } from "@alfalab/core-components/amount";
import { Link } from "react-router-dom";

export const Product = ({id, preview, title, price, subtitle, images, description, sizes, colors, stickerNumbers, models}: ProductType) => {
  const item = {title, price, description, images, sizes, colors, stickerNumbers, models};
  return (
    <div className="product-card">
      <Link to={`/product-information/${id}`} state={item}>
        <img src={preview} alt={title} className="product-img" />
        <div className="product-text">
          <p>{title}</p>
          <p className="product-text-subtitle">{subtitle}</p>
          <p>
            <Amount value={price} currency="RUR" minority={0} />
          </p>
        </div>
      </Link>
    </div>
  );
};
