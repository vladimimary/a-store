import { Location } from 'react-router-dom';

export type ProductType = {
    id: number;
    preview: string;
    title: string;
    price: number;
    subtitle?: string;
    availability?: boolean;
    images: Array<string>;
    description: string;
    colors?: Array<string>;
    sizes?: Array<string>;
    stickerNumbers?: Array<number>;
    models?:Array<string>;
}

export type ProductsType = {
    products: Array<ProductType>;
    title?: string;
    description?: string;
}

export type ProductsTitleType = {
    title: string;
    subtitle?: string;
}

export type ProductsGroupType = {
    head: ProductsTitleType,
    allProducts?: Array<ProductsType>,
}

export interface ProductLocation extends Location {
    state: ProductType
}
