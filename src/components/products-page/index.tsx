import { useParams } from "react-router-dom"
import './index.css'
import { ProductsTitle } from "./products-title";
import { ProductsList } from "./products-list";
import { getGroup } from "../../utils";
import { ProductsGroupType } from "./types";



export const ProductsPage = () => {
    
    const { productsGroup } = useParams(); 
    let group : ProductsGroupType = {head: { title:"Страница не найдена"}}

    if (productsGroup !== undefined) {group = getGroup(productsGroup)}

    return (

        <div className="products-wrapper">
            <ProductsTitle title={group.head.title} subtitle={group.head.subtitle} />
            { group.allProducts &&
            <div className='products-list-card'>
                { group.allProducts.map((item, id) => <ProductsList key={id} products={item.products} title={item.title} description={item.description} />) }
            </div> }
        </div>
    )
}