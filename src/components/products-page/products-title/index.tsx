import { ProductsTitleType } from "../types"

export const ProductsTitle = ({title, subtitle} : ProductsTitleType) => {
    return(
        <div className='products-wrapper-head'>
                <h1 className='products-wrapper-h products-wrapper-h-title'>{title}</h1>
                <h4 className='products-wrapper-h products-wrapper-h-subtitle'>{subtitle}</h4>
        </div>
    )
    
}