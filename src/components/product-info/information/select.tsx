import {
  BaseSelectChangePayload,
  Select,
} from "@alfalab/core-components/select";
import { ArrayForSelectType } from "../types";
import { useState } from "react";

export const SelectArray = ({
  arr,
  label,
  handleChangeFocusImage,
}: ArrayForSelectType) => {
  const arrObj = arr.map((item, id) => ({ key: `${id}`, content: `${item}` }));
  const [value, setValue] = useState<string | null>(arrObj[0].key);
  const handleChange = ({ selected }: BaseSelectChangePayload) => {
    setValue(selected ? selected.key : null);
    if (handleChangeFocusImage && selected) {
      handleChangeFocusImage(+selected.key);
    }
  };

  return (
    <div>
      <p className="product-text-label">{label}</p>
      <Select
        options={arrObj}
        placeholder="Выберите элемент"
        name="select"
        multiple={false}
        onChange={handleChange}
        selected={value}
      />
    </div>
  );
};
