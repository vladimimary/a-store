import { Amount } from "@alfalab/core-components/amount";
import { Button } from "@alfalab/core-components/button";
import "./index.css";
import { SelectArray } from "./select";
import { ProductInformationType } from "../types";
import { useAppDispatch } from "../../../store";
import { addProduct } from "../../../store/reducers";


export const Information = ({
  img,
  handleChangeFocusImage,
  title,
  price,
  description,
  sizes,
  colors,
  stickerNumbers,
  models,
}: ProductInformationType) => {

  const dispatch = useAppDispatch();
  const plusHandleClick = () => {
    const New = {
      id:"123",
  title: title,
  price: price,
  img: "http://qa-games.ru/astore/public/images/51168667.png",
  size: "xs",
  color: "red",
  stickerNumber: 2,
  model: "fgfg",
  count: 2
    }

    dispatch(addProduct(New));
  };


  return (
    <div>
      <div className="product-text product-text-grid">
        <p>{title}</p>
        <p>
          <Amount value={price} currency="RUR" minority={0} />
        </p>
        {sizes && <SelectArray arr={sizes} label="Размер" />}
        {colors && (
          <SelectArray
            arr={colors}
            label="Цвет"
            handleChangeFocusImage={handleChangeFocusImage}
          />
        )}
        {stickerNumbers && (
          <SelectArray arr={stickerNumbers} label="Номер стикера" />
        )}
        {models && <SelectArray arr={models} label="Модель" />}
        <Button view="primary" onClick={plusHandleClick}>В корзину</Button>
        <p className="product-text-description">{description}</p>
      </div>
    </div>
  );
};
