import { ImagesType } from "./types";

export const Galery = ({
  images,
  focusImage,
  handleChangeFocusImage,
}: ImagesType) => {

  const handleClickOnImage = (id:number) => () => handleChangeFocusImage(id);

  return (
    <div className="galery">
      <img
        src={images[focusImage]}
        alt="изображение товара"
        className="galery-main"
        data-testid="mainImage"
      />
      <div className="galery-thumbnails">
        {images.map((item, id) => (
          <img
            src={item}
            key={id}
            alt="изображение товара"
            className="galery-thumbnail"
            onClick={handleClickOnImage(id)}
            data-testid={id}
          />
        ))}
      </div>
    </div>
  );
};
