export type ImagesType = {
  images: Array<string>;
  focusImage: number;
  handleChangeFocusImage: (id: number) => void;
};
