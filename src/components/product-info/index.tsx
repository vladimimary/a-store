import "./index.css";
import { useLocation } from "react-router-dom";
import { ProductLocation } from "../products-page/types";
import { Galery } from "./galery";
import { Information } from "./information";
import { useState } from "react";


export const ProductInfo = () => {
  const location: ProductLocation = useLocation();
  const { images, ...info } = location.state;

  const [focusImage, setFocusImage] = useState(0);
  const handleChangeFocusImage = (id: number) => {
    setFocusImage(id);
  };
  

  return (
    <div className="product-info">
      <Galery images={images} focusImage={focusImage} handleChangeFocusImage={handleChangeFocusImage}/>
      <Information handleChangeFocusImage={handleChangeFocusImage} {...info} />
    </div>
  );
};