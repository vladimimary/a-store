import { Location } from "react-router-dom";


export type ProductInformationType = {
  img?:string;
  handleChangeFocusImage: (id: number) => void;
  title: string;
  price: number;
  description: string;
  sizes?: Array<string>;
  colors?: Array<string>;
  stickerNumbers?: Array<number>;
  models?:Array<string>;
};

export type ArrayForSelectType = {
  arr: Array<string> | Array<number>;
  label: string;
  handleChangeFocusImage?: (id: number) => void;
}
