import './index.css'

export const Footer = () => {
    return(
        <footer data-testid="footer">
            © ООО "Альфа Фьюче Пипл", {new Date().getFullYear()}
        </footer>
    )
    
    };