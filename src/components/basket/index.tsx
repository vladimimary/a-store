import { useAppSelector } from "../../store";
import { BasketItem } from "./basket-item";
import { CustomButton } from "@alfalab/core-components/custom-button";
import "./index.css";
import { Amount } from "@alfalab/core-components/amount";
import { RecipientInfo } from "./recipient-info";
import { Divider } from "@alfalab/core-components/divider";
import React from "react";

export const Basket = () => {
  const basket = useAppSelector((state) => state.basket.list);
  const sum = basket.reduce(
    (sum, current) => sum + current.price * current.count,
    0
  );
  const [isShowRecipientInfo, setIsShowRecipientInfo] = React.useState(false);
  const handleModalOpen = React.useCallback(() => setIsShowRecipientInfo((prev) => !prev), []);

  return (
    <div className="basket">
      <h3 className="title">Ваш заказ</h3>
      <div className="basket-colums">
        <div>
          {basket.map((item) => (
            <BasketItem {...item} />
          ))}
        </div>
        <div className="basket-result">
          <CustomButton size="xl" backgroundColor="#000" onClick={handleModalOpen}>
            Дальше
          </CustomButton>
          <div className="basket-sum">
            <p>
              Сумма: <Amount value={sum} currency="RUR" minority={0} />{" "}
            </p>
            <p>
              Доставка по России:{" "}
              <Amount value={350} currency="RUR" minority={0} />
            </p>
            <p className="basket-sum-total">
              Итоговая сумма:
              <Amount value={sum + 350} currency="RUR" minority={0} />
            </p>
          </div>
        </div>
      </div>
      <Divider />
      {isShowRecipientInfo && <RecipientInfo/>}
    </div>
  );
};
