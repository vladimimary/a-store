export type BasketItemType = {
  id:string;
  title: string;
  price: number;
  img: string;
  size?: string;
  color?: string;
  stickerNumber?: number;
  model?: string;
  count: number;
};
