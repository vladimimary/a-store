import { Divider } from "@alfalab/core-components/divider";
import { BasketItemType } from "./types";
import "./index.css";
import { Amount } from "@alfalab/core-components/amount";
import { IconButton } from "@alfalab/core-components/icon-button";
import { MinusMIcon } from "@alfalab/icons-glyph/MinusMIcon";
import { AddMIcon } from "@alfalab/icons-glyph/AddMIcon";
import { CrossMIcon } from "@alfalab/icons-glyph/CrossMIcon";
import { useAppDispatch } from "../../../store";
import { addCount, deleteCount, deleteProduct } from "../../../store/reducers";
import { Circle } from "@alfalab/core-components/icon-view/circle";

export const BasketItem = ({
  id,
  title,
  price,
  img,
  size,
  color,
  stickerNumber,
  model,
  count,
}: BasketItemType) => {
  const dispatch = useAppDispatch();
  const plusHandleClick = () => {
    dispatch(addCount(id));
  };
  const minusHandleClick = () => {
    count > 1 ? dispatch(deleteCount(id)) : dispatch(deleteProduct(id));
  };
  const deleteHandleClick = () => {
    dispatch(deleteProduct(id));
  };

  return (
    <>
      <Divider />
      <div className="item item-grid">
        <img src={img} alt="изображение товара" className="item-thumbnail" />
        <div>
          <h4 className="item-h">{title}</h4>
          <div className="item-text">
            {size && <p>Размер: {size}</p>}
            {color && <p>Цвет: {color}</p>}
            {stickerNumber && <p>Номер стикера: {stickerNumber}</p>}
            {model && <p>Модель: {model}</p>}
          </div>
        </div>
        <div className="item-count">
          <div>
            <Circle size={24}>
              {" "}
              <IconButton
                view="primary"
                size="xxs"
                icon={MinusMIcon}
                onClick={minusHandleClick}
              />
            </Circle>
          </div>
          <text className="item-h">{count}</text>
          <div>
            <Circle size={24}>
              <IconButton
                view="primary"
                size="xxs"
                icon={AddMIcon}
                onClick={plusHandleClick}
              />
            </Circle>
          </div>
        </div>
        <div>
          <Amount
            className="item-h"
            value={price}
            currency="RUR"
            minority={0}
          />
        </div>
        <div>
          <Circle size={24}>
            {" "}
            <IconButton
              view="primary"
              size="xxs"
              icon={CrossMIcon}
              onClick={deleteHandleClick}
            />
          </Circle>
        </div>
      </div>
    </>
  );
};
