import { InputWithLabel } from './input-with-top-label';

export const RecipientInfo = () => {
  return (
    <div className='recipient-info'>
      <InputWithLabel fieldName='fio' fieldLabel='ФИО'  />
      <InputWithLabel fieldName='email' fieldLabel='e-mail' type='email'/>
      <InputWithLabel fieldName='phone' fieldLabel='Телефон' type='tel' />
      <InputWithLabel fieldName='address' fieldLabel='Адрес(если вы выбрали самовывоз - оставьте поле пустым)' />
      <InputWithLabel fieldName='promo' fieldLabel='Промокод' />
      <InputWithLabel fieldName='comment' fieldLabel='Комментарий к заказу' />
    </div>
  );
};
