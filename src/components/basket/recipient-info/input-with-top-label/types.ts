export type InputWithLabelType = {
    fieldName: string;
    fieldLabel: string;
    type?: "number" | "text" | "tel" | "email" | "card" | "money" | "password" | undefined; 
}