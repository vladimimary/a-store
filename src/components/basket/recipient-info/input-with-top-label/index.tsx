import { Input } from "@alfalab/core-components/input";
import { InputWithLabelType } from "./types";
import './index.css'

export const InputWithLabel = ({
  fieldName,
  fieldLabel,
  type
}: InputWithLabelType) => {
  return (
    <div>
      <p className="label-input">{fieldLabel}</p>
      {type ? <Input name={fieldName} block={true} type={type} /> : <Input name={fieldName} block={true} /> }
      <br />
    </div>
  );
};
