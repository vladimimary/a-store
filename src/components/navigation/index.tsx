import { Link } from '@alfalab/core-components/link'
import './index.css'
import { Circle } from '@alfalab/core-components/icon-view/circle';
import { IconButton } from '@alfalab/core-components/icon-button';
import { MailMIcon } from '@alfalab/icons-glyph/MailMIcon';
import { PhoneMIcon } from '@alfalab/icons-glyph/PhoneMIcon';
import { CrossMWhiteIcon } from '@alfalab/icons-glyph/CrossMWhiteIcon';
import { WhatsappMIcon } from '@alfalab/icons-logotype/WhatsappMIcon';
import React from 'react'
import { ButtonProps } from './types';

export const Navigation = React.memo(({handleClick}:ButtonProps) => {
        return(
        <div className="navigation">
            <div>
                <div id="close"><IconButton view='primary' icon= {CrossMWhiteIcon} size='s' onClick={handleClick}/></div>
                <Link
                    view="default"
                    rel="noopen"
                    href="/products/sdelano-v-alfe"
                >
                    <p className="navigation-text">Сделано в Альфе</p>
                </Link>
                <Link
                    view="default"
                    rel="noopen"
                    href="/products/svoy-dizain"
                >
                    <p className="navigation-text">Свой дизайн</p>
                </Link>
                <Link
                    view="default"
                    rel="noopen"
                    href="/contacts"
                >
                    <p className="navigation-text">Контакты</p>
                </Link>
            </div>
            <div>
                <Link
                    view="default"
                    rel="noopen"
                    href="/"
                >
                    <p className="navigation-text navigation-text-addit">Политика кофиденцальность и обработки персональных данных</p>
                </Link>
                <Link
                    className='navigation-link'
                    view="default"
                    rel="noopen"
                    href="/"
                >
                    <Circle size={32} backgroundColor="#ffffff">
                        <MailMIcon color="black"/>
                    </Circle>
                </Link>
                <Link
                    className='navigation-link'
                    view="default"
                    rel="noopen"
                    href="/"
                >
                    <Circle size={32} backgroundColor="#ffffff">
                        <PhoneMIcon color="black"/>
                    </Circle>
                </Link>
                <Link
                    className='navigation-link'
                    view="default"
                    rel="noopen"
                    href="/"
                >
                    <Circle size={32} backgroundColor="#ffffff">
                        <WhatsappMIcon color='black'/>
                    </Circle>
                </Link>
            </div>
        </div>
    )
})