import { MenuCard} from "./menu-card"

export const Menu = () => {
    const img2 = process.env.PUBLIC_URL + '/assets/image/fon1.jpeg'
    const img1 = process.env.PUBLIC_URL + '/assets/image/fon2.jpeg'
    return (
        <>
        <MenuCard image={img1} text="Сделано в Альфе" link="/products/sdelano-v-alfe"/>
        <MenuCard image={img2} text="Свой дизайн" link="/products/svoy-dizain"/>
        </>
    )
}