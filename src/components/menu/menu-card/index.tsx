import './index.css'
import { Link } from 'react-router-dom'

type MenuCardType ={
    image: string;
    text: string;
    link: string;
}

export const MenuCard = ({ image, text, link }: MenuCardType) => {
    return (
        <div className="menu-card">
            <Link to={link}>
                <img className='menu-img' src={image} alt="example"/>
                <p className='menu-text'>{text}</p>
            </Link>
        </div>
    )
}
