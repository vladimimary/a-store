import { Outlet } from "react-router-dom"
import { BasketIndicator } from "../basket-indicator"
import { Footer } from "../footer"
import { Header } from "../header"

export const Page = () => {
    return (
        <>
            <Header />
            <BasketIndicator />
            <Outlet />
            <Footer />
        </>
    )
}