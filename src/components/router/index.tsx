import { Route, Routes} from "react-router-dom"
import { Page } from "../page"
import { Main } from "../main"
import { ProductInfo } from "../product-info"
import { ProductsPage } from "../products-page"
import { EmptyPage } from "../emty-page"
import { Contacts } from "../contacts"
import { Basket } from "../basket"

export const AppRoute = () => {
    return (
        <Routes>
                <Route path="/" element={ <Page /> }>
                    <Route index element={<Main />} />
                    <Route path="/products/:productsGroup" element={ <ProductsPage /> } />
                    <Route path="/contacts" element={ <Contacts /> } />
                    <Route path="/basket" element={ <Basket /> } />
                    <Route path="/product-information/:id" element={<ProductInfo/>} />
                    <Route path="*" element={<EmptyPage/>} />
                </Route>
        </Routes>
    )
}