import React from 'react';
import { BrowserRouter  as Router} from 'react-router-dom';
import { AppRoute } from "../router"

import './index.css';

export const App = () => (
        <Router>
            <AppRoute />
        </Router>
);

