import { ProductsGroupType } from "../components/products-page/types";
import ProductsCustom from "../components/products-page/groups.json";
import ProductsCommon from "../components/products-page/products.json";

export function getGroup(productsGroup: string) {
  const group: ProductsGroupType =
    productsGroup === "sdelano-v-alfe"
      ? {
          head: {
            title: "Сделано в Альфе",
            subtitle: "Хотим каждую из этих вещей! Себе, родным и друзьям",
          },
          allProducts: [ProductsCommon],
        }
      : productsGroup === "svoy-dizain"
      ? {
          head: {
            title: "Свой дизайн",
            subtitle:
              "Выберите вещь, а затем - цвет, размер и стикер.\nПеренесём стикер на вещь как на фото",
          },
          allProducts: ProductsCustom.groups,
        }
      : {
          head: {
            title: "Страница не найдена",
          },
        };

        return group;
}
